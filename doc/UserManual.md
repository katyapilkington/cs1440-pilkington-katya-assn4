##User Manual: Fractal Painter
Note* The fractal painter is intended to be run from the terminal. In the form
# python path/to/src/main.py path/to/fractalFile.frac [gradient]
To run the fractal painter:
1. Open the terminal
2. Type "python " Do not hit enter at this time.
3.  Type the path to the main.py file followed by a space. Do not hit enter at this time.
    (example: "projectFolder/src/main.py ")
4.  Type the path to the file containing the fractal information you wish to use. Do not press enter at this time
**(example: projectFolder/data/elephants.frac)
5. (Optional) type the name of the desired gradient. If no gradient or a non-existed gradient is provided, the program
    will run the defaul (Grayscale) gradient. Current supported gradients are: Grayscale, Red, Green, Blue,
     and GreenToBlue
6. Press enter. The program should then begin to run and a status bar will appear in the terminal.
As the image is being created, the status bar will update and the percentage will rise. When the bar is full and
the percentage reaches 100%, a window will pop up and display the fractal image (you may have to click on the window to
bring it up). As well, a PNG file should be written in the src folder.

** (Many options can be found under the data file contained with this project. You may use your own fractal file as long
  as the information follows the same pattern as those provided.)