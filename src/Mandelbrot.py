from Fractal import Fractal


class Mandelbrot(Fractal):
    def __init__(self, iterations):
        self.__z = complex(0, 0)
        self.__iterations = iterations

    def count(self, x, y):
        z = self.__z  # c0
        for value in range(1, self.__iterations + 1):
            z = z * z + complex(x, y)  # get c1, c2...
            if abs(z) > 2:
                return value    # return cIteration, where the sequence in unbounded
        return self.__iterations       # Indicate a bounded sequence (max cIteration)
