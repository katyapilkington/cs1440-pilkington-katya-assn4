from Mandelbrot import Mandelbrot
from Julia import Julia
from Mandelbrot4 import Mandelbrot4

def createFractal(configuration):
    if configuration.get("type") == "mandelbrot":
        fractal = Mandelbrot(configuration.get("iterations"))
    elif configuration.get("type") == "julia":
        fractal = Julia(configuration.get("creal"), configuration.get("cimag"), configuration.get("iterations"))
    elif configuration.get("type") == "mandelbrot4":
        fractal = Mandelbrot4()
    else:
        raise NotImplementedError("Invalid fractal requested")

    return fractal
