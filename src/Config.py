import sys
def dictionaryFromFile(file):
    fileDict = dict()
    with open(file) as dataFile:
        for line in dataFile:
            if line.startswith("#"):
                continue
            line = line.lower()
            line = line.rstrip()
            line = line.replace(" ", "")
            lines = line.split(":", 1)
            fileDict[lines[0]] = lines[1]
    return fileDict


def configurationDictionary(file):
    dictionary = dictionaryFromFile(file)
    configure = dict()
    errors = list()
    if "type" in dictionary:
        type(dictionary, configure)

    if "pixels" in dictionary:
        addIntType(configure, dictionary, "pixels")
    else:
        errors.append("pixels")

    if "centerx" in dictionary:
        addFloatType(configure, dictionary, "centerx")
    else:
        errors.append("centerX")

    if "centery" in dictionary:
        addFloatType(configure, dictionary, "centery")
    else:
        errors.append("centerY")

    if "axislength" in dictionary:
        addFloatType(configure, dictionary, "axislength")
    else:
        errors.append("axisLength")

    if "iterations" in dictionary:
        addIntType(configure, dictionary, "iterations")
    else:
        errors.append("iterations")

    if len(errors) > 0:
        throwErrors(errors)
    else:
        finalConfig(configure)
        return configure


def type(dictionary, configure):
    if dictionary.get("type") == "julia":
        configure["type"] = "julia"
        if "creal" in dictionary and "cimag" in dictionary:
            addFloatType(configure, dictionary, "creal")
            addFloatType(configure, dictionary, "cimag")
    else:
        configure["type"] = dictionary.get("type")


def addIntType(configDict, fileDict, string):
    configDict[string] = int(fileDict.get(string))


def addFloatType(configDict, fileDict, string):
    configDict[string] = float(fileDict.get(string))


def throwErrors(errorList):
    print("The configure file has errors in the following sections:")
    for item in errorList:
        print(item)
    sys.exit()


def finalConfig(configDict):
    halfAxis = configDict.get("axislength") / 2

    minPoint = [configDict.get("centerx") - halfAxis, configDict.get("centery") - halfAxis]
    configDict["min"] = minPoint

    maxPoint = [configDict.get("centerx") + halfAxis, configDict.get("centery") + halfAxis]
    configDict["max"] = maxPoint

    pixelSize = configDict.get("axislength") / configDict.get("pixels")
    configDict["pixelsize"] = pixelSize
