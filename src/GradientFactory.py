from oneColorGradients import Grayscale, Red, Green, Blue, GreenToBlue


gradientDictionary = {
    "grayscale": Grayscale(),
    "red": Red(),
    "green": Green(),
    "blue": Blue(),
    "greentoblue": GreenToBlue()}

def createGradient(arguments):
    if len(arguments) < 3 or str(arguments[2]).lower() not in gradientDictionary:
        gradient = Grayscale()
        print("Creating default color gradient")

    else:
        gradient = gradientDictionary[str(arguments[2]).lower()]
        print("Creating " + arguments[2] + " color gradient")

    return gradient
