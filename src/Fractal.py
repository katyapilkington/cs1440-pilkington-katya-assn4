from abc import ABC, abstractmethod


class Fractal(ABC):
    pass

    @abstractmethod
    def count(self):
        pass

    # def createIterationList(self):
    #     iterationList = []
    #     for row in range(self.__maxIterations):
    #         iterationList.append([])
    #         for column in range(self.__maxIterations):
    #             x = self.__minX + column * self.__pixelSize
    #             y = self.__minY + row * self.__pixelSize
    #             iterationValue = self.IterationCount(x, y)
    #             iterationList[row].append(iterationValue)
    #     return iterationList
