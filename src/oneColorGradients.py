from LinearGradient import LinearGradient
from Color import Color


class Grayscale(LinearGradient):
    def __init__(self, steps=100):
        super().__init__(steps, Color(), Color(255, 255, 255))


class Red(LinearGradient):
    def __init__(self, steps=100):
        super().__init__(steps, Color(), Color(225, 0, 0))


class Green(LinearGradient):
    def __init__(self, steps=100):
        super().__init__(steps, Color(), Color(0, 255, 0))


class Blue(LinearGradient):
    def __init__(self, steps=100):
        super().__init__(steps, Color(), Color(0, 0, 255))


class GreenToBlue(LinearGradient):
    def __init__(self, steps=100):
        super().__init__(steps, Color(0, 255, 0), Color(0, 0, 255))
