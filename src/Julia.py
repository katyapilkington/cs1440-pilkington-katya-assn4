from Fractal import Fractal


class Julia(Fractal):
    def __init__(self, cReal, cImaginary, iterations):
        self.__z = complex(cReal, cImaginary)
        self.__iterations = iterations

    def count(self, x, y):
        z = self.__z  # c0
        for value in range(1, self.__iterations + 1):
            z = z * z + complex(x, y)  # get c1, c2...
            if abs(z) > 2:
                return value    # return cIteration, where the sequence in unbounded
        return self.__iterations       # Indicate a bounded sequence (max cIteration)

    # def iterationCount(self, x, y):
    #     super().iterationCount(x, y)
    #
    # def createIterationList(self):
    #     super().createIterationList()

# def juliaIterationCount(cReal, cImaginary, iterations, x, y):
#     c = complex(cReal, cImaginary)  # c0
#     for i in range(1, iterations + 1):
#         c = c * c + complex(x, y)  # get c1, c2...
#         if abs(c) > 2:
#             return i    # return cIteration, where the sequence in unbounded
#     return iterations       # Indicate a bounded sequence (max cIteration)
#
#
# def createIterationListJ(pixels, iteration, cReal, cImaginary, minX, minY, pixelSize):
#     iterationList = []
#     for row in range(pixels):
#         iterationList.append([])
#         for column in range(pixels):
#             x = minX + column * pixelSize
#             y = minY + row * pixelSize
#             iterationValue = juliaIterationCount(cReal, cImaginary, iteration, x, y)
#             iterationList[row].append(iterationValue)
#     return iterationList
#
#
# def printIterationList(iterationList):
#     for row in iterationList:
#         for column in row:
