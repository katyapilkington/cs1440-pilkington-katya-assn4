import sys
from tkinter import Tk, Canvas, PhotoImage, mainloop


def createIterationList(configuration, fractal):
    iterationList = []
    for row in range(configuration.get("pixels")):
        iterationList.append([])
        for column in range(configuration.get("pixels")):
            x = configuration.get("min")[0] + column * configuration.get("pixelsize")
            y = configuration.get("min")[1] + row * configuration.get("pixelsize")
            iterationValue = fractal.count(x, y)
            iterationList[row].append(iterationValue)
    return iterationList


def paint(configuration, fractal, gradient):
    image, window = createImage(configuration.get("pixels"))
    iterationList = createIterationList(configuration, fractal)
    for col in range(configuration.get("pixels")):
        statusBarLoading(col, configuration.get("pixels"))
        for row in range(configuration.get("pixels")):
            color = pixelColor(iterationList[col][row], gradient)  # TODO: get iteration from mandelbrot or julia?
            image.put(color, (col, row))
    finishedStatusBar(configuration.get("pixels"))
    createCanvas(configuration.get("pixels"), gradient, image, window)
    writeImage(image)


def pixelColor(iteration, gradient):
    return gradient.getColor(iteration - 1)


def statusBarLoading(column, pixels):
    ratio = int(pixels / 64)
    if column % ratio == 0:
        pips = column // ratio
        percentage = column / pixels
        print(f" ({pixels}x{pixels}) {'=' * pips}{'_' * (64 - pips)} {percentage:.0%}", end='\r', file=sys.stderr)


def finishedStatusBar(pixels):
    print(f"({pixels}x{pixels}) ================================================================ 100%",
          file=sys.stderr)


    # Display the image on the screen
def createImage(pixels):
    window = Tk()
    image = PhotoImage(width=pixels, height=pixels)
    return image, window


def createCanvas(pixels, gradient, image, window):
    canvas = Canvas(window, width=pixels, height=pixels, bg=gradient.getFirstColor())
    canvas.pack()
    canvas.create_image((pixels // 2, pixels // 2), image=image, state="normal")


def writeImage(image):
    image.write("fractal.png")
    print(f"Wrote image fractal.png")
    mainloop()
