from Color import Color
from Gradient import Gradient


# Todo: Make a gradient class that takes arguments N steps (being the # of interations in the frac file)
# and beginning/ending colors(?). Return/create a list of Colors from beginning to last color (currently white to black
class LinearGradient(Gradient):
    # TODO: refactor gradient class to take colors that do not have the same value for r,g,&b each iteration
    def __init__(self, steps, beginningColor, endingColor):
        self. __beginningColor = beginningColor
        self.__endingColor = endingColor
        self.__steps = steps
        self.__gradientArray = self.makeArray()

    def makeArray(self):
        gradient = [self.__beginningColor]

        redValue = self.__beginningColor.getRed()
        greenValue = self.__beginningColor.getGreen()
        blueValue = self.__beginningColor.getBlue()

        redRatio = self.redRatio()
        greenRatio = self.greenRatio()
        blueRatio = self.blueRatio()

        for i in range(1, self.__steps - 1):
            redValue = int(redValue + redRatio)
            greenValue = int(greenValue + greenRatio)
            blueValue = int(blueValue + blueRatio)
            gradient.append(self.createColor(redValue, greenValue, blueValue))
        gradient.append(self.__endingColor)
        return gradient

    def redRatio(self):
        if self.__endingColor.getRed() and self.__beginningColor.getRed():
            return 0
        else:
            return (self.__endingColor.getRed() - self.__beginningColor.getRed()) / self.__steps

    def greenRatio(self):
        if self.__endingColor.getGreen() and self.__beginningColor.getGreen():
            return 0
        else:
            return (self.__endingColor.getGreen() - self.__beginningColor.getGreen()) / self.__steps

    def blueRatio(self):
        if self.__endingColor.getBlue() and self.__beginningColor.getBlue():
            return 0
        else:
            return (self.__endingColor.getBlue() - self.__beginningColor.getBlue()) / self.__steps

    def createColor(self, red, green, blue):
        color = Color(red, green, blue)
        return color

    def getArray(self):
        return self.__gradientArray

    def getFirstColor(self):
        return self.__gradientArray[0]

    def getLastColor(self):
        return self.__gradientArray[-1]

    def getColor(self, step):
        return self.__gradientArray[step]

    def getNumberOfColors(self):
        return len(self.__gradientArray)


if __name__ == '__main__':
    grad = Gradient(100).getArray()

    for i in range(len(grad)):
        print(grad[i])