import sys
from Config import configurationDictionary
from GradientFactory import createGradient
from FractalFactory import createFractal
from ImagePainter import paint


def main():
    if len(sys.argv) < 2:
            raise NotImplementedError("Please supply a fractal file as the first argument")
    configureDict = configurationDictionary(sys.argv[1])
    fractal = createFractal(configureDict)
    gradient = createGradient(sys.argv)
    paint(configureDict, fractal, gradient)


main()
