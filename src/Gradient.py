from abc import ABC, abstractmethod

class Gradient:

    @abstractmethod
    def getFirstColor(self):
        pass

    @abstractmethod
    def getLastColor(self):
        pass

    @abstractmethod
    def getColor(self):
        pass

    @abstractmethod
    def getNumberOfColors(self):
        pass
